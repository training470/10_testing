## Molecule
Установка из домашней папки пользователя `vagrant`
```shell
molecule init role skillbox -d delegated
```
Возможно папка с molecule не прописана в Path. Решение: 
```shell
PATH=$PATH:/home/vagrant/.local/bin
```  
/home/vagrant/skillbox/tasks/main.yml
```yaml
---
# tasks file for skillbox
- name: Ensure nginx installed
  package:
    name: "nginx"
    state: present
```
/home/vagrant/skillbox/defaults/verify.yml
```yaml
- name: Verify
  hosts: all
  tasks:
          - name: Check nginx binary
            stat:
                    path: "/usr/sbin/nginx"
            register: this
            failed_when: "not this.stat.exists"
```
## Karate
Установка
```commandline
mvn archetype:generate -DarchetypeGroupId=com.intuit.karate -DarchetypeArtifactId=karate-archetype -DarchetypeVersion=0.9.6 -DgroupId=com.softwarethatmatters.karate.maven -DartifactId=karate-maven-setup
```
## Testinfra + Ansible
```commandline
cd /vagrant
pytest test_simple.py
```
## Ansible Lint. Пример тестирования playbook
### Использование в текущем репозитории
```commandline
cd /vagrant/ansible-lint
ansible-lint -p AB/deploy.yml -v -q
ansible-lint -p СС/deploy.yml -v -q
```
### Примеры использования
Сканирование каталога ansible-lint/examples ansible-lint:
```commandline
ansible-lint -p examples/playbooks/example.yml
```
Если playbooks включают другие playbooks или роли:
```commandline
ansible-lint --force-color --offline -p examples/playbooks/include.yml
```
Отчёт выполнения прохождения тестов в формате json через команду codeclimate ansible-lint:
```commandline
ansible-lint -f codeclimate examples/playbooks/norole.yml
```