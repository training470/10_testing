def test_os_release(host):
 assert host.file("/etc/os-release").contains("Ubuntu")

def test_sshd_inactive(host):
 assert host.service("sshd").is_running is True